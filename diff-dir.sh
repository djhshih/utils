#!/bin/bash

if (( $# < 2 )); then
	echo "usage: ${0##/*} <directory1> <directory2>" >&2
	exit 1
fi

dir1=$1
dir2=$2

for f in $(ls -1 $dir1); do
	if [[ ! -f $dir2/$f ]]; then
		echo "> $dir1/$f"
		echo "< $dir2/$f"
	else
		wc -l $dir1/$f $dir2/$f
		#diff $dir1/$f $dir2/$f
	fi
done
