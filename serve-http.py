#!/usr/bin/env python3

import http.server
import socketserver

# minimal web server.  serves files relative to the
# current directory.

port = 8888

Handler = http.server.SimpleHTTPRequestHandler

httpd = socketserver.TCPServer(("", port), Handler)

print("serving at port", port)
httpd.serve_forever()

