#!/usr/bin/env Rscript

library(argparser);
library(io);
library(dplyr);
library(stringr);

pr <- arg_parser("Get annotation file from Firehose") %>%
	add_argument("sample_annot", help="sample annotation file") %>%
	add_argument("fh_annot", help="Firehose annotation file") %>%
	add_argument("field", help="Field name of file paths") %>%
	add_argument("target_dir", help="target directory") %>%
	add_argument("--id", help="id type", default="sample_id") %>%
	add_argument("--fext", help="file extension") %>%
	add_argument("--overwrite", help="overwrite target file", flag=TRUE);


argv <- parse_args(pr);
#argv <- parse_args(pr, c("~/exigens/brain-mets/annot/sample-info_wes_stage2.tsv", "fiss/brain-mets_ice_callstats.tsv", "call_stats_capture", "mutect"));

sample_annot <- qread(argv$sample_annot, stringsAsFactors=FALSE);
fh_annot <- qread(argv$fh_annot, stringsAsFactors=FALSE);

# only query samples that exist in both sample_annot and fh_annot
annot <- inner_join(sample_annot, fh_annot, by=c("fh_pair_id" = "pair_id"));

get_fext <- function(x) {
	substring(str_extract(x, "\\..+"), 2)
}


dir.create(argv$target_dir, showWarnings=FALSE, recursive=TRUE);

success <- unlist(mapply(
	function(sample_id, path) {
		if (is.na(sample_id)) {
			NULL
		} else if (!is.na(path) && str_length(path) > 0) {
			if (is.na(argv$fext)) {
				fext <- get_fext(basename(path));
			} else {
				fext <- argv$fext;
			}
			target <- as.character(filename(sample_id,
				date=NA, time=NA,
				path=argv$target_dir, ext=fext
			));

			if (argv$overwrite) {
				copy <- TRUE;
			} else {
				if (file.exists(target)) {
					if (!is.na(file.info(path)$mtime) &&
							!is.na(file.info(target)$mtime) &&
							file.info(path)$mtime > file.info(target)$mtime) {
						# source file is newer
						copy <- TRUE;
						message("Newer: ", path)
					} else {
						# source file is not newer or date information is missing
						copy <- FALSE;
					}
				} else {
					copy <- TRUE;
				}
			}

			if (copy) {
				success <- file.copy(path, target, overwrite=TRUE, copy.date=TRUE);
				if (!success) {
					message("Error: ", path)
				}
			} else {
				success <- TRUE;
			}

			success
		} else {
			NA
		}
	},
	annot[[argv$id]],
	annot[[argv$field]]
));


message()
message("Succeeded:")
message(paste(names(success)[which(success)], collapse="\n"), "\n")

message("Invalid:")
message(paste(names(success)[is.na(success)], collapse="\n"), "\n")

message("Failed:")
message(paste(names(success)[which(!success)], collapse="\n"), "\n")

message("Warnings:")
warnings()
