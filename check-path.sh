#!/bin/bash

infile=$1

if (( $# < 1 )); then
	echo "usage: ${0##*/} <file-list.txt>" >&2
	exit 1
fi

for f in $(cat $infile); do
	[[ ! -f $f ]] && echo "$f"
done
