#!/bin/bash

# Clean qsub output files

set -euo pipefail
#IFS=$'\n\t'

if (( $# > 1 )); then
	path=$1
else
	path=$HOME
fi

echo "Files found: "
echo

files=$(ls -1 $path | grep -E '\.[eo][0-9]+')

echo $files | column
echo

echo -n "Delete all [y/n]?  "
read response

if [[ $response == "y" ]]; then
	rm -f $files
fi

