#!/bin/bash

# Clean up status files

rm_if_not_exist() {

	# path to the status file
	status=$1

	# split path into directory and file
	dir=${status%/*}
	file=${status##*/}

	# determine the target file:
	#   strip off prefix '.' and suffix '.done'
	tmp=${file%.done}
	target=${tmp:1}

	# delete status file if target does not exist
	if [[ ! -f $dir/$target ]]; then
		echo "rm $status"
		rm $status
	fi
}

find . -name '*.done' \
	| while read file; do
		rm_if_not_exist "$file"
	done

