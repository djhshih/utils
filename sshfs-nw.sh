#!/bin/bash

set -eu

if (( $# < 1 )); then
	echo "usage: $0 <home|share>"
	exit 1
fi

target=$1

if [[ $target == "home" ]]; then

	remote=northwestern:/home/djh4534
	mountpoint=$HOME/sshfs/nw/home

elif [[ $target == "share" ]]; then

	remote=northwestern:/home/djh4534/share
	mountpoint=$HOME/sshfs/nw/share

else

	echo "Invalid argument: unknown target" >&2
	exit 1

fi

# check if mountpoint is already mounted
if mount | grep $mountpoint > /dev/null; then
	# force unmount
	case "$(uname -s)" in
		Linux) umount='fusermount -u -z';;
		Darwin) umount='diskutil umount force';;
		*) umount='umount';;
	esac
	$umount $mountpoint
fi

# create mountpoint directory, if it does not already exist
if [[ ! -d $mountpoint ]]; then
	mkdir -p $mountpoint
fi

# mount remote storage
sshfs $remote $mountpoint -o reconnect,follow_symlinks

